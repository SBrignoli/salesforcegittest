public with sharing class K_Availability_Controller {

    @AuraEnabled(cacheable=true)
    public static map<string, list<Stock>> getStockData(String quoteId) {

        System.debug('Current USER--> '+UserInfo.getName());

        SBQQ__Quote__c quote= [SELECT id, Default_Warehouse__c, Default_Warehouse__r.Name FROM SBQQ__Quote__c WHERE id =: quoteId];


        List<Id> productsId             = new List<Id>();
        List<Id> lineId                 = new List<Id>();
        List<Id> warehouseId            = new List<Id>();
        List<String> availabilityTitle  = new List<String>();
        map<Id, string> productsName    = new map<Id, string>();
        list<string> listProdNoAva      = new list<string>();
        map<Id, Id> warehouseLine       = new map<Id, Id>();
        List<SBQQ__QuoteLine__c> quoteLineList =[SELECT id, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Product__r.Name, Specific_Warehouse__c, AvailabilityTitle__c, CreatedDate FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteId ORDER BY CreatedDate ASC];
        // List<SBQQ__QuoteLine__c> quoteLineList =[SELECT id, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Product__r.Name, Specific_Warehouse__c, AvailabilityTitle__c, CreatedDate FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteId AND SBQQ__Product__r.Name != 'Additional Charges' ORDER BY CreatedDate ASC];

        for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
            productsId.add(quoteLine.SBQQ__Product__c);
            lineId.add(quoteLine.id);
            availabilityTitle.add(quoteLine.AvailabilityTitle__c);
            productsName.put(quoteLine.id, quoteLine.AvailabilityTitle__c); 
            if(quoteLine.Specific_Warehouse__c != null){
                warehouseLine.put(quoteLine.Id, quoteLine.Specific_Warehouse__c);
                warehouseId.add(quoteLine.Specific_Warehouse__c);
            }else{
                warehouseLine.put(quoteLine.Id, quote.Default_Warehouse__c);
                warehouseId.add(quote.Default_Warehouse__c);
            }
        }

        map<string, list<Stock>> stockMap = new map<string, list<Stock>>();
        map<string, list<Stock>> stockMapReturn = new map<string, list<Stock>>();

        list<Id> lineAvailable      = new list<Id>();
        list<Id> lineNotAvailable   = new list<Id>();

        for (Stock__c stock : [SELECT id, Item_in_stock__c, ID_BPCS__c, Warehouse_code__c FROM Stock__c WHERE ID_BPCS__c IN : productsId]){

            for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
                if(quoteLine.SBQQ__Product__c==stock.ID_BPCS__c && warehouseLine.get(quoteLine.id) == stock.Warehouse_code__c && stock.Item_in_stock__c >= quoteLine.SBQQ__Quantity__c){
                    lineAvailable.add(quoteLine.id);     
                }
            }
        }

        for(integer i = 0; i < lineId.size() ; i ++){  
            if(!lineAvailable.contains(lineId[i])){
                lineNotAvailable.add(lineId[i]);
                listProdNoAva.add(productsName.get(lineId[i]));
            }
        }

        for (Stock__c stock : [SELECT id, Item_in_stock__c, ID_BPCS__c, Warehouse_code__c, Warehouse_code__r.Name FROM Stock__c WHERE Warehouse_code__c IN : warehouseId AND ID_BPCS__c IN : productsId]){

            stock stockRec = new stock();
            stockRec.itemInStock = stock.Item_in_stock__c;
            stockRec.warehouseName = stock.Warehouse_code__r.Name;

            for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
                if(quoteLine.SBQQ__Product__c==stock.ID_BPCS__c && warehouseLine.get(quoteLine.id) == stock.Warehouse_code__c){
                    List<Stock> stockList = new List<Stock>();
                    stockList.add(stockRec);
                    stockMap.put(quoteLine.AvailabilityTitle__c, stockList);         
                }
            }
        }

        system.debug('@@ LIST NOT AVAILABLE-->'+ lineNotAvailable);
        for(id lineNotAva : lineNotAvailable){
            list<Stock> stockProd = stockMap.get(productsName.get(lineNotAva)); 
            if(!stockMap.keySet().contains(productsName.get(lineNotAva)) || stockProd[0].itemInStock ==null){
                system.debug('@@ DENTRO IF');
                List<Stock> stockList = new List<Stock>();
                stock stockRec = new stock();
                stockRec.itemInStock = 0;
                stockRec.warehouseName = quote.Default_Warehouse__r.Name;
                stockList.add(stockRec);
                stockMap.put(productsName.get(lineNotAva), stockList);                 
            }
        }
        system.debug('@@ STOCKMAP-->'+ stockMap);

        for (Stock__c stock : [SELECT id, Warehouse_code__c,Warehouse_code__r.Name, ID_BPCS__r.Name, Item_in_stock__c  FROM Stock__c WHERE ID_BPCS__c IN : productsId ORDER BY Item_in_stock__c DESC]){
            
            stock stockRec = new stock();
            stockRec.itemInStock = stock.Item_in_stock__c;
            stockRec.warehouseName = stock.Warehouse_code__r.Name;

            for(SBQQ__QuoteLine__c quoteLine : quoteLineList){

                if(quoteLine.SBQQ__Product__c==stock.ID_BPCS__c && stock.Item_in_stock__c >= quoteLine.SBQQ__Quantity__c && warehouseLine.get(quoteLine.id) != stock.Warehouse_code__c){
                    if(stockMap.containsKey(quoteLine.AvailabilityTitle__c)) {
                        List<Stock> stockList = stockMap.get(quoteLine.AvailabilityTitle__c);
                        stockList.add(stockRec);
                        stockMap.put(quoteLine.AvailabilityTitle__c, stockList);                     
                    } else {
                        stockMap.put(quoteLine.AvailabilityTitle__c, new List<Stock> { stockRec }); 
                    }
                }
            }
        }

        system.debug('@@ STOCKMAP2-->'+ stockMap);

        for(String productName : availabilityTitle){
            if(!productName.contains('DoNotShow')){
                System.debug('@@ product name-->'+productName);
                List<Stock> value = stockMap.get(productName);
                string key;
                integer position = availabilityTitle.indexOf(productName) + 1;
                if(listProdNoAva.contains(productName)){
                    key = Integer.valueOf(position) +' - '+ productName + ' - Not Available';
                } else{
                    key = Integer.valueOf(position) +' - '+ productName + ' - Available';
                }
                stockMapReturn.put(key,value);
            }
        }

        System.debug('@@ stockMap-->'+stockMap);
        return stockMapReturn;
    }

    public class Stock {
        @AuraEnabled public String warehouseName;
        @AuraEnabled public Decimal itemInStock;
    }

}